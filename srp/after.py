class FormatData:
    def __init__(self, raw_data):
        self.raw_data = raw_data

    def preapre(self):
        result = ''
        for item in self.raw_data:
            new_line = ','.join(
                (
                    item['name'],
                    item['surname'],
                    item['occupation']
                )
            )
            result += f"{new_line}\n"
        return result
class FileWriter:
    def __init__(self, filename):
        self.filname = filename
    def write(self, data):
        with open(self.filname, 'w', encoding='UTF8') as f:
            f.write(data)

data = [
    {
        'name': 'Sherlock',
        'surname': ' Holmes',
        'occupation': ' private detective'
    },
    {
        'name': 'John',
        'surname': ' Nolan',
        'occupation': ' ofc'
    }
]

formatter = FormatData(data)
formatted_data = formatter.preapre()

writer = FileWriter("out.csv")
writer.write(formatted_data)