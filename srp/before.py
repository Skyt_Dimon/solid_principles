class ExportCsv:
    def __init__(self, raw_data):
        self.data = self.preapre(raw_data)

    def preapre(self, raw_data):
        result = ''
        for item in raw_data:
            new_line = ','.join(
                (
                    item['name'],
                    item['surname'],
                    item['occupation']
                )
            )
            result += f"{new_line}\n"
        return result

    def write_file(self, filename):
        with open(filename, 'w', encoding='UTF8') as f:
            f.write(self.data)

data = [
    {
        'name': 'Sherlock',
        'surname': ' Holmes',
        'occupation': ' private detective'
    },
    {
        'name': 'John',
        'surname': ' Nolan',
        'occupation': ' ofc'
    }
]

exporter = ExportCsv(data)
print(exporter.preapre(data))
exporter.write_file('out.csv')